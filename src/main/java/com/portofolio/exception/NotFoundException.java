package com.portofolio.exception;

public class NotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public NotFoundException() {
		super();
	}
	
	public NotFoundException(String message) {
		 super(message);
	}
	
	public NotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
