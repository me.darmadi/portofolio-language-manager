package com.portofolio.exception;

public class CustomException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public CustomException() {
		super();
	}
	
	public CustomException(String message) {
		 super(message);
	}
	
	public CustomException(String message, Throwable cause) {
		super(message, cause);
	}
}
