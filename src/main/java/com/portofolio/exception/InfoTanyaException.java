package com.portofolio.exception;

public class InfoTanyaException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7884228681064640342L;

	public InfoTanyaException() {
		super();
	}
	
	public InfoTanyaException(String message) {
		 super(message);
	}
	
	public InfoTanyaException(String message, Throwable cause) {
		
	}
	
}
